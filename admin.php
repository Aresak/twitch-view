<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 20.06.2017
 * Time: 8:52
 */

require_once "../user.injection.php";

if (isset($_GET["hidentity"])) {
    include "../aresaktwitchmodule.php";
    $sql = atm::sql();
    $ui = new userinjection();
    $user = $ui->getHidentity($sql);

    $hdr = "?hidentity=" . $_GET["hidentity"];
    $h_raw = $_GET["hidentity"];

    switch ($user->usergroup_id) {
        case "1":
            $atm = new atm();



            $members = mysqli_query($sql, "SELECT * FROM sbr500_members WHERE streamerorviewer='2'")
            or die(mysqli_error($sql));


            $problem = "";
            $streamers = array();
            $badURL = array();
            for ($i = 0; $i < mysqli_num_rows($members); $i++) {
                $r = mysqli_result($members, $i, "twitch_url");

                if (!($r == "http://|" || $r == "")) {
                    if(sizeof(explode("/", explode("|", $r)[0])) != 4) {
                        $a = count($badURL);
                        $badURL[$a]["id"] = mysqli_result($members, $i, "id");
                        $badURL[$a]["url"] = $r;
                        $badURL[$a]["username"] = mysqli_result($members, $i, "username");
                    }
                    $c = explode("/", explode("|", $r)[0])[3];
                    $f = false;
                    foreach ($streamers as $streamer) {
                        if ($streamer == $c) {
                            $f = true;
                            break;
                        }
                    }
                    if (!$f) $streamers[count($streamers)] = explode("/", explode("|", $r)[0])[3];
                } else {
                    $problem .= $r . " - " . mysqli_result($members, $i, "username") . " ||| ";
                }
            }


            $priority = mysqli_query($sql, "SELECT * FROM atm_twitchview_priority")
            or die(mysqli_error($sql));


            $prioritized = array();
            $plebs = array();

            foreach ($streamers as $streamer) {
                $f = false;
                for ($i = 0; $i < mysqli_num_rows($priority); $i++) {
                    if(mysqli_result($priority, $i, "twitch") == $streamer) {
                        $f = true;
                        break;
                    }
                }

                if($f)
                    $prioritized[sizeof($prioritized)] = $streamer;
                else
                    $plebs[sizeof($plebs)] = $streamer;
            }
            ?>
            <html>
            <head>
                <title>DragonsGetIt | TwitchView Admin</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
                      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
                      crossorigin="anonymous">
                <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                        crossorigin="anonymous"></script>
                <script src="/TwitchScout/cookie.js"></script>
            </head>
            <body>
            <h1>
                DragonsGetIt | TwitchView Admin
            </h1>

            <div id="support">
                <style>
                    #support {
                        position: fixed;
                        top: 15px;
                        right: 15px;
                    }

                    li {
                        padding: 2px 5px;
                        border: 1px solid black;
                        background-color: rgba(229, 227, 215, 1);
                    }

                    .highlighted {
                        background-color: rgba(222, 229, 160, 1);
                    }
                </style>
                <i><b>Made by Aresak</b><br>
                    for DragonsGetIt.com</i><br>
                <i>
                    <small>Waddap <?php echo $user->fullname ?></small>
                </i>
                <script>
                    $( function() {
                        $( "#priority, #streamers" ).sortable({
                            connectWith: ".connectedSortable",
                            cancel: ".permanent",
                            placeholder: "highlighted",
                            change: function( event, ui ) {

                            }
                        }).disableSelection();
                    } );

                    function sortTest() {
                        console.log($( ".connectedSortable" ).sortable( "toArray" ));
                    }

                    function save() {
                        if(confirm("Are you sure you want to save new priority list?")) {
                            var list = $( ".connectedSortable" ).sortable( "toArray" );
                            var strigified = "";
                            for(var i = 0; i < list.length; i ++) {
                                strigified += list[i] + ":";
                            }

                            $.get("prioritize.php", {hidentity: "<?php echo $h_raw; ?>", data: "<?php echo $_GET["data"]; ?>", list: strigified, action: "save"}, function(data) {
                                console.log(data);
                            }, "html");
                        }
                    }
                </script>
            </div>

            <br>
            <table id="list" class="table">
                <tr>
                    <td>
                        <button onclick="save();" style="position: relative; width: 100%; left: 0;">
                            SAVE!
                        </button>
                    </td><td></td>
                </tr>
                <tr>
                    <td align="center"><b>PRIORITIZED STREAMS</b></td>
                    <td align="center"><b>UNSORTED STREAMS</b></td>
                </tr>
                <tr>
                    <td align="right">
                        <ul id="priority" class="connectedSortable">
                            <li id="dragonsgetit" class="prioritized permanent">DragonsGetIt</li>
                            <?php
                            foreach($prioritized as $p) {
                                echo "<li id='$p' class='prioritized'>$p</li>";
                            }
                            ?>
                        </ul>
                    </td>
                    <td align="left">
                        <ul id="streamers" class="connectedSortable">
                            <?php
                            foreach($plebs as $p) {
                                echo "<li id='$p' class='pleb'>$p</li>";
                            }
                            ?>
                        </ul>
                    </td>
                </tr>
            </table>
            <br>
            <h2>Members with invalid twitch url</h2>
            <table class="table table-striped">
                <tr>
                    <td><b>ID</b></td>
                    <td><b>Username</b></td>
                    <td><b>URL</b></td>
                </tr>
                <?php

                foreach($badURL as $member) {
                    echo "<tr>";
                    echo "<td>" . $member["id"] . "</td>";
                    echo "<td>" . $member["username"] . "</td>";
                    echo "<td>" . $member["url"] . "</td>";
                    echo "</tr>";
                }

                ?>
            </table>
            </body>
            </html>
            <?php
            break;
        default:
            echo "For Administrators only";
            break;
    }
} else {
    $ui = new userinjection();
    $ui->getHeaderIdentity("TwitchScout/TwitchView/admin.php", null);
}
<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 10.06.2017
 * Time: 16:48
 */

session_start();

if(isset($_GET["twitchview"])) {
    if(isset($_GET["type"])) {
        switch ($_GET["type"]) {
            case "autoplay":
                if (isset($_COOKIE["twitchview_autoplay"])) {
                    setcookie("twitchview_autoplay", !$_COOKIE["twitchview_autoplay"], time()+60*60*24*360);
                } else setcookie("twitchview_autoplay", false, time()+60*60*24*360);

                echo $_COOKIE["twitchview_autoplay"];
                break;
            case "muted":
                if (isset($_COOKIE["twitchview_muted"])) {
                    setcookie("twitchview_muted", !$_COOKIE["twitchview_muted"], time()+60*60*24*360);
                } else setcookie("twitchview_muted", false, time()+60*60*24*360);

                echo !$_COOKIE["twitchview_muted"];
                break;
        }
    }
}
<html>
    <head>
        <title>Editor | #DragonsGetIt live</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
        <div id="editor">
            <div id="settings">
                <table class="table table-striped">
                    <tr>
                        <td align="right">
                            <b>iFrame Width</b>
                        </td>
                        <td align="left">
                            <input class="property" type="text" property="iframe-width" value="100%">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>iFrame Height</b>
                        </td>
                        <td align="left">
                            <input class="property" type="text" property="iframe-height" value="100%">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>LiveBox Width</b>
                        </td>
                        <td align="left">
                            <input class="property" type="text" property="livebox-width" value="322px">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>LiveBox Height</b>
                        </td>
                        <td align="left">
                            <input class="property" type="text" property="livebox-height" value="280px">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>LiveBoxes in row</b>
                        </td>
                        <td align="left">
                            <input class="property" type="number" property="cols" value="5">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>Max LiveBoxes on page</b>
                        </td>
                        <td align="left">
                            <input class="property" type="number" property="livebox-max" value="0">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>Sort by</b>
                        </td>
                        <td align="left">
                            <select class="property" property="sort-by">
                                <option value="views-asc" selected>Views Ascending</option>
                                <option value="views-desc">Views Descending</option>
                                <option value="followers-asc">Followers Ascending</option>
                                <option value="followers-desc">Followers Descending</option>
                                <option value="viewers-asc">Viewers Ascending</option>
                                <option value="viewers-desc">Viewers Descending</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>Window Refresh</b> (0 for not refresh)
                        </td>
                        <td align="left">
                            <input class="property" type="number" property="window-refresh" value="5">
                        </td>
                    </tr>
                </table>
            </div>
            <div id="result">
                <button onclick="refresh();">Refresh</button> <button onclick="generate();">Generate</button>
                <div id="info">

                </div>
                <hr>
                <textarea id="result-raw"></textarea>
            </div>
        </div>
        <div id="preview" align="center"></div>
    </body>
</html>

<script>
    function generate() {
        var res = "<iframe src=\'http://dragonsgetit.com/TwitchScout/TwitchView/view.php?";
        var width = "";
        var height = "";
        $(".property").each(function() {
            res += $(this).attr("property") + "=" + $(this).val() + "&";
            switch ($(this).attr("property")) {
                case "iframe-width":
                    width = $(this).val();break;
                case "iframe-height":
                    height = $(this).val();break;
            }
        });
        res += "\' style='width: " + width + ";height: " + height +";'></iframe>";
        $("#preview").html(res);
        $("#result-raw").val(res);
        console.log(res);
    }

    function refresh() {

    }
</script>

<style>
    #editor {
        position: fixed;
        top: 0;
        left: 0;
        width: 50%;
        height: 95%;
    }

    #settings {
        position: fixed;
        top: 0;
        width: 100%;
        height: 75%;
        border-bottom: 1px solid black;
    }

    #result {
        position: fixed;
        top: 75%;
        width: 100%;
        height: 25%;
    }

    #preview {
        position: fixed;
        top: 0;
        right: 0;
        width: 50%;
        height: 100%;
        border-left: 1px solid black;
        background-color: rgba(239, 232, 137, 1);
    }
</style>
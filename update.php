<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 08.06.2017
 * Time: 12:07
 */
include "../smartblock.php";
$update_time = 86400;
function mysqli_result($result, $row, $field = 0)
{
    if ($result === false) return false;
    if ($row >= mysqli_num_rows($result)) return false;
    if (is_string($field) && !(strpos($field, ".") === false)) {
        $t_field = explode(".", $field);
        $field = -1;
        $t_fields = mysqli_fetch_fields($result);
        for ($id = 0; $id < mysqli_num_fields($result); $id++) {
            if ($t_fields[$id]->table == $t_field[0] && $t_fields[$id]->name == $t_field[1]) {
                $field = $id;
                break;
            }
        }
        if ($field == -1) return false;
    }
    mysqli_data_seek($result, $row);
    $line = mysqli_fetch_array($result);
    return isset($line[$field]) ? $line[$field] : false;
}
$cid = "";
$f = rtrim(
    mcrypt_decrypt(
        MCRYPT_RIJNDAEL_128,
        hash('sha256', "", true),
        substr(base64_decode("/=="), mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
        MCRYPT_MODE_CBC,
        substr(base64_decode("/=="), 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC))
    ),
    "\0"
);
$b = explode("|", $f);
$sql = mysqli_connect($b[0], $b[2], $b[1], $b[2])
    or die(mysqli_error($sql));

$result = mysqli_query($sql, "SELECT * FROM sbr500_members WHERE streamerorviewer='2'")
or die(mysqli_error($sql));


$problem = "";
$streamers = array();
for ($i = 0; $i < mysqli_num_rows($result); $i++) {
    $r = mysqli_result($result, $i, "twitch_url");

    if (!($r == "http://|" || $r == "")) {
        $c = explode("/", explode("|", $r)[0])[3];
        $f = false;
        foreach ($streamers as $streamer) {
            if ($streamer == $c) {
                $f = true;
                break;
            }
        }
        if (!$f) $streamers[count($streamers)] = explode("/", explode("|", $r)[0])[3];
    } else {
        $problem .= $r . " - " . mysqli_result($result, $i, "username") . " ||| ";
    }
}


// load
$streamers_sb = new SmartBlock("Streamers");
$streamers_sb->process(file_get_contents("data.sb"));

$streamer_data = array();
for($i = 0; $i < $streamers_sb->getProperty("length"); $i ++) {
    $streamer_data[$i]["name"] = $streamers_sb->getProperty($i . "_name");
    $streamer_data[$i]["id"] = $streamers_sb->getProperty($i . "_id");
    $streamer_data[$i]["logo"] = $streamers_sb->getProperty($i . "_logo");
    $streamer_data[$i]["offline_image"] = $streamers_sb->getProperty($i . "_offline");
    $streamer_data[$i]["banner"] = $streamers_sb->getProperty($i . "_banner");
    $streamer_data[$i]["updated_at"] = $streamers_sb->getProperty($i . "_updated");

    if($streamers_sb->getProperty($i . "_updated") <= (time() + $update_time)) {
        // update
        $channel = json_decode(file_get_contents("https://api.twitch.tv/kraken/channels/" . $streamers_sb->getProperty($i . "_name") . "?client_id=" . $cid), true);
        $streamer_data[$i]["logo"] = $channel["logo"];
        $streamer_data[$i]["offline_image"] = $channel["video_banner"];
        $streamer_data[$i]["banner"] = $channel["profile_banner"];
        $streamer_data[$i]["updated_at"] = time();
    }
}

foreach($streamers as $streamer) {
    $found = false;
    foreach($streamer_data as $sdata) {
        if($sdata["name"] == $streamer) {
            $found = true;
            break;
        }
    }

    if(!$found) {
        $b = sizeof($streamer_data);
        $channel = json_decode(file_get_contents("https://api.twitch.tv/kraken/channels/" . $streamer . "?client_id=" . $cid), true);
        $streamer_data[$b]["name"] = $channel["name"];
        $streamer_data[$b]["id"] = $channel["_id"];
        $streamer_data[$b]["logo"] = $channel["logo"];
        $streamer_data[$b]["offline_image"] = $channel["video_banner"];
        $streamer_data[$b]["banner"] = $channel["profile_banner"];
        $streamer_data[$b]["updated_at"] = time();
    }
}


// save
$save_data = new SmartBlock("Streamers");
$c = 0;
foreach($streamer_data as $data) {
    if(empty($data["id"]))
        continue;

    $save_data->addProperty($c . "_id", $data["id"]);
    $save_data->addProperty($c . "_name", $data["name"]);
    $save_data->addProperty($c . "_updated", $data["updated_at"]);
    $save_data->addProperty($c . "_logo", $data["logo"]);
    $save_data->addProperty($c . "_offline", $data["offline_image"]);
    $save_data->addProperty($c . "_banner", $data["banner"]);
    $c ++;
}

$save_data->addProperty("length", $c);

file_put_contents("data.sb", $save_data->process());
